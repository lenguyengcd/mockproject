﻿namespace MockProject.Data.Common
{
    public abstract record Response<TData> : Response
    {
        public TData? Data { get; init; }
    }
    public abstract record Response
    {
        public string? Message { get; init; }
        public bool Success { get; init; }
        public Error[] Errors { get; init; } = Array.Empty<Error>();
        public string? ErrorCode { get; set; }
    }
    public record Error(string Code, string Message);
}
