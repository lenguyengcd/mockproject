﻿namespace MockProject.Data.Common
{
    public class UserClaimsConstant
    {
        public const string UserInformation = "UserInformation";
    }
    public class ErrorMessages
    {
        public static string ValidationMessage = "Data invalid.";
        public static string ValidationPageMessage = "Page must greater than 0.";
        public static string ValidationPageSizeMessage = "Page must greater than 0.";
    }
    public class ErrorCodes
    {
        public static string ValidationErrorCode = "600";
    }
    public class PolicyConst
    {
        public const string AdminPolicy = nameof(AdminPolicy);
    }
    public class RoleConst
    {
        public const string Administrator = nameof(Administrator);
        public const string Moderator = nameof(Moderator);
        public const string User = nameof(User);
    }

    public class UserConst
    {
        public const string AdministratorId = "7a726a97-e017-4244-b9ee-8bdeb49d456a";
        public const string AdministratorUserName = "systemadmin";
    }
    public class CharacterTypeConst
    {
        public const string Knight = nameof(Knight);
        public const string Magic = nameof(Magic);
        public const string Assassin = nameof(Assassin);
    }
}
