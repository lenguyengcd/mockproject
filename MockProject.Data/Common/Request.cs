﻿using MediatR;

namespace MockProject.Data.Common
{
    public interface IValidationRequest<TResponse, TModel> : IRequest<TResponse>
        where TResponse : Response
    {
        public TModel Model { get; init; }
    }
}
