﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MockProject.Data.Common;
using MockProject.Model.Entities;

namespace MockProject.Data.Migrations
{
    public class DbInitializer
    {
        private readonly ModelBuilder modelBuilder;

        public DbInitializer(ModelBuilder modelBuilder)
        {
            this.modelBuilder = modelBuilder;
        }

        public void Seed()
        {
            modelBuilder.Entity<ApplicationRole>().HasData(
                   new ApplicationRole() { Id = new Guid("d3fcb385-6ce4-4f6c-bf81-d2eea98bd477"), Name = RoleConst.Administrator,NormalizedName= RoleConst.Administrator },
                   new ApplicationRole() { Id = new Guid("8e62be19-bc77-4073-bafb-5e11d9bbc75a"), Name = RoleConst.Moderator, NormalizedName = RoleConst.Moderator },
                   new ApplicationRole() { Id = new Guid("8449aa1d-d8ed-404a-ac19-62adfe0e2f10"), Name = RoleConst.User, NormalizedName = RoleConst.User }
                   );
            modelBuilder.Entity<ApplicationUser>().HasData(
                new ApplicationUser
                {
                    Id = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a"),
                    UserName = "systemadmin",
                    NormalizedUserName = "systemadmin",
                    PasswordHash = "AFtvh0GyFq43cZ5LTofr+gxJU3UUArhuoKVQCdjmVPsvWwjY4+atnuMIB/m5t9eTmQ==",
                    InsertedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    InsertedById = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a"),
                    UpdatedById = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a")
                });
            modelBuilder.Entity<ApplicationUserRole>().HasData(
             new ApplicationUserRole
             {
                 RoleId = new Guid("d3fcb385-6ce4-4f6c-bf81-d2eea98bd477"),
                 UserId = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a")
             });

            modelBuilder.Entity<CharacterType>().HasData(
           new CharacterType()
           {
               Id = new Guid("bb1cf6ec-7e21-4b5f-be82-b9d560f94d86"),
               Name = CharacterTypeConst.Knight,
               InsertedAt = DateTime.Now,
               UpdatedAt = DateTime.Now,
               InsertedById = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a"),
               UpdatedById = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a"),
               MagicDefault = 20,
               StrengthDefault = 20,
               VitalityDefault = 30
           },
           new CharacterType()
           {
               Id = new Guid("ec72ccbf-eaca-4912-9d5e-9f9829407893"),
               Name = CharacterTypeConst.Magic,
               InsertedAt = DateTime.Now,
               UpdatedAt = DateTime.Now,
               InsertedById = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a"),
               UpdatedById = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a"),
               MagicDefault = 30,
               StrengthDefault = 20,
               VitalityDefault = 20
           },
           new CharacterType()
           {
               Id = new Guid("5366d5ed-a733-4a54-8557-2a4814943e25"),
               Name = CharacterTypeConst.Assassin,
               InsertedAt = DateTime.Now,
               UpdatedAt = DateTime.Now,
               InsertedById = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a"),
               UpdatedById = new Guid("7a726a97-e017-4244-b9ee-8bdeb49d456a"),
               MagicDefault = 20,
               StrengthDefault = 30,
               VitalityDefault = 20
           }
           );

        }
    }

}
