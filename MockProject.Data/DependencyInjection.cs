﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MockProject.Data.Infrastructure;
using MockProject.Model.Entities;

namespace MockProject.Data
{
    public static partial class DependencyInjection
    {
        public static void AddDataInjection(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<IMockProjectDbContext, MockProjectDbContext>(op =>
                op.UseLazyLoadingProxies().UseSqlServer(configuration.GetConnectionString("Default"),
                b => b.MigrationsAssembly(typeof(MockProjectDbContext).Assembly.FullName)))
            ;
            services.AddIdentity<ApplicationUser, ApplicationRole>(options => options.SignIn.RequireConfirmedAccount = true)
            .AddEntityFrameworkStores<MockProjectDbContext>();

            services.AddScoped<IApplicationSignInManager, ApplicationSignInManager>();
            services.AddScoped<IIdentityUser, IdentityUser>();
            services.AddScoped<IMockProjectUnitOfWork, MockProjectUnitOfWork>();
            var assembly = typeof(DependencyInjection).Assembly;
      
        }
    }
}
