﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Data.Infrastructure
{
    public class IdentityUser : IIdentityUser
    {
        private readonly IApplicationSignInManager _signInManager;

        public IdentityUser(IApplicationSignInManager signInManager)
        {
            _signInManager = signInManager;
        }

        public Guid UserId => _signInManager.CurrentUserId;
        public string UserName => _signInManager.CurrentUserName;
    }
}
