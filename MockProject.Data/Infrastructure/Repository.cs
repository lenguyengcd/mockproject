﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
namespace MockProject.Data.Infrastructure
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbContext _dbContext;
        protected readonly DbSet<T> _dbset;
        public Repository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _dbset = dbContext.Set<T>();
        }

        public void Add(T entity)
        {
            _dbset.Add(entity);
        }

        public void Delete(T entity)
        {
            _dbset.Remove(entity);
        }

        public void Delete(Expression<Func<T, bool>> expression)
        {
            _dbset.RemoveRange(Gets(expression));
        }

        public IQueryable<T> Gets()
        {
           return _dbset.AsQueryable();
        }

        public IQueryable<T> Gets(Expression<Func<T, bool>> expression)
        {
           return _dbset.Where(expression).AsQueryable();
        }

        public void Update(T entity)
        {
            _dbset.Update(entity);
        }
    }
}
