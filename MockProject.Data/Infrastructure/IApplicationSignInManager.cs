﻿namespace MockProject.Data.Infrastructure
{
    public interface IApplicationSignInManager
    {
        Guid CurrentUserId { get; }
        string CurrentUserName { get; }
    }
}

