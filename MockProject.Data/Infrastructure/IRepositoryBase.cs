﻿using System.Linq.Expressions;

namespace MockProject.Data.Infrastructure
{
    public interface IRepositoryBase<T>
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity, bool isHardDelete = false);
        void Delete(Expression<Func<T, bool>> expression, bool isHardDelete = false);
        IQueryable<T> Gets();
        IQueryable<T> Gets(Expression<Func<T, bool>> expression);
    }
}
