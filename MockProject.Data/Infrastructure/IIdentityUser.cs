﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Data.Infrastructure
{
    public interface IIdentityUser
    {
        Guid UserId { get; }
        string UserName { get; }
    }
}
