﻿using MockProject.Model.Entities;

namespace MockProject.Data.Infrastructure
{
    public class MockProjectUnitOfWork : IMockProjectUnitOfWork
    {
        private readonly MockProjectDbContext _dbContext;
        private readonly IIdentityUser _curretUser;
        public MockProjectUnitOfWork(MockProjectDbContext dbContext, IIdentityUser user)
        {
            _dbContext = dbContext;
            _curretUser = user;
        }

        public IMockProjectDbContext DbContext => _dbContext;
        private readonly IRepository<ApplicationUser> _userRepository;
        public IRepository<ApplicationUser> UserRepository => _userRepository ?? new Repository<ApplicationUser>(_dbContext);

        private readonly IRepository<ApplicationRole> _roleRepository;
        public IRepository<ApplicationRole> RoleRepository => _roleRepository ?? new Repository<ApplicationRole>(_dbContext);
        private readonly IRepository<ApplicationUserRole> _userRoleRepository;
        public IRepository<ApplicationUserRole> UserRoleRepository => _userRoleRepository ?? new Repository<ApplicationUserRole>(_dbContext);
        private readonly IRepositoryBase<CharacterType> _characterTypeRepository;
        public IRepositoryBase<CharacterType> CharacterTypeRepository => _characterTypeRepository ?? new RepositoryBase<CharacterType>(_dbContext, _curretUser);
        private readonly IRepositoryBase<Character> _characterRepository;
        public IRepositoryBase<Character> CharacterRepository => _characterRepository ?? new RepositoryBase<Character>(_dbContext, _curretUser);
        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
    }
}
