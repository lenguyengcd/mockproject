﻿using Microsoft.EntityFrameworkCore;
using MockProject.Model.SeedWork;
using System.Linq.Expressions;
using Z.EntityFramework.Plus;

namespace MockProject.Data.Infrastructure
{
    public class RepositoryBase<T> : IRepositoryBase<T>
        where T : class, IEntityBase
    {
        protected readonly DbContext dbContext;
        protected readonly DbSet<T> dbset;
        private readonly IIdentityUser _currentUser;
        public RepositoryBase(DbContext dbContext, IIdentityUser currentUser)
        {
            this.dbContext = dbContext;
            dbset = this.dbContext.Set<T>();
            _currentUser = currentUser;
        }

        public virtual void Add(T entity)
        {
            entity.Id = Guid.NewGuid();
            entity.InsertedAt = DateTime.Now;
            entity.UpdatedAt = DateTime.Now;
            entity.InsertedById = _currentUser.UserId;
            entity.UpdatedById = _currentUser.UserId;
            dbset.Add(entity);
        }

        public void Delete(T entity, bool isHardDelete = false)
        {
            if (isHardDelete)
            {
                dbset.Remove(entity);
            }
            else
            {
                entity.UpdatedAt = DateTime.Now;
                entity.UpdatedById = _currentUser.UserId;
                entity.IsDeleted = true;
                Update(entity);
            }
        }

        public void Delete(Expression<Func<T, bool>> expression, bool isHardDelete = false)
        {
            if (!isHardDelete && typeof(T) == typeof(EntityBase))
            {
                dbset.Where(expression).
                   Update(x => new EntityBase { IsDeleted = true });
            }
            else
            {
                dbset.RemoveRange(Gets(expression));
            }
        }

        public IQueryable<T> Gets()
        {
           return dbset.AsQueryable();
        }

        public IQueryable<T> Gets(Expression<Func<T, bool>> expression)
        {
            return dbset.Where(expression);
        }

        public void Update(T entity)
        {
            entity.UpdatedAt = DateTime.Now;
            entity.UpdatedById = _currentUser.UserId;
            dbset.Update(entity);
        }
    }
}
