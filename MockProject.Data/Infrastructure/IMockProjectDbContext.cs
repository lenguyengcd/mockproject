﻿using Microsoft.EntityFrameworkCore;
using MockProject.Model.Entities;

namespace MockProject.Data.Infrastructure
{
    public interface IMockProjectDbContext : IDisposable
    {
        public DbSet<CharacterType> CharacterTypes {get;}
        public DbSet<Character> Characters {get;}
        public DbSet<ApplicationUser> ApplicationUsers {get;}
        //public DbSet<ApplicationUserRole> ApplicationUserRoles {get;}
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
