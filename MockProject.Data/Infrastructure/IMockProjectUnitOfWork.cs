﻿using MockProject.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Data.Infrastructure
{
    public interface IMockProjectUnitOfWork : IDisposable
    {
        IMockProjectDbContext DbContext { get; }
        IRepository<ApplicationUser> UserRepository { get; }
        IRepository<ApplicationRole> RoleRepository { get; }
        IRepository<ApplicationUserRole> UserRoleRepository { get; }
        IRepositoryBase<CharacterType> CharacterTypeRepository { get; }
        IRepositoryBase<Character> CharacterRepository { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
