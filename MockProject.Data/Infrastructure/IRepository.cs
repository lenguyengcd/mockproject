﻿using System.Linq.Expressions;

namespace MockProject.Data.Infrastructure
{
    public interface IRepository<T>
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(Expression<Func<T,bool>> expression);
        IQueryable<T> Gets();
        IQueryable<T> Gets(Expression<Func<T, bool>> expression);

    }
}
