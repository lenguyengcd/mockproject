﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace MockProject.Data.Infrastructure
{
    public class MockProjectDbContextFactory : IDesignTimeDbContextFactory<MockProjectDbContext>
    {
        public MockProjectDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<MockProjectDbContext>();
            optionsBuilder.UseSqlServer("Server=localhost;Database=MockProject;Trusted_Connection=true;");

            return new MockProjectDbContext(optionsBuilder.Options);
        }
    }
}
