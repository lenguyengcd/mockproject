﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MockProject.Data.Common;
using MockProject.Model.Entities;
using MockProject.Model.Models;
using System.Text.Json;

namespace MockProject.Data.Infrastructure
{
    public class ApplicationSignInManager : IApplicationSignInManager
    {
        private readonly IHttpContextAccessor _contextAccessor;

        public ApplicationSignInManager(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        private UserClaim _currentUser;
        public UserClaim CurrentUser
        {
            get {
                if(_currentUser !=null)
                return _currentUser;
                var user = _contextAccessor.HttpContext.User.Claims.FirstOrDefault(f=>f.Type == UserClaimsConstant.UserInformation)?.Value;
                if (string.IsNullOrEmpty(user)) return null;
                _currentUser = JsonSerializer.Deserialize<UserClaim>(user);
                return _currentUser;
            }
        }

        public Guid CurrentUserId => CurrentUser!=null ? CurrentUser.Id : new Guid(UserConst.AdministratorId);
         
        public string CurrentUserName => CurrentUser != null ? CurrentUser.Username : UserConst.AdministratorUserName;
    }
}
