﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MockProject.Data.Migrations;
using MockProject.Model.Entities;

namespace MockProject.Data.Infrastructure
{
    public partial class MockProjectDbContext  : IdentityDbContext<
        ApplicationUser, ApplicationRole, Guid,
        IdentityUserClaim<Guid>, ApplicationUserRole, IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>, IMockProjectDbContext
    {
        public MockProjectDbContext(DbContextOptions<MockProjectDbContext> options) :base(options) { }

        public DbSet<CharacterType> CharacterTypes => Set<CharacterType>();
        public DbSet<Character> Characters => Set<Character>();
        public DbSet<ApplicationUser> ApplicationUsers => Set<ApplicationUser>();
        //public DbSet<ApplicationUserRole> ApplicationUserRoles => Set<ApplicationUserRole>();

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return base.SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CharacterType>(entity =>
            {
                entity.HasOne(d => d.UpdatedBy)
                   .WithMany()
                   .HasForeignKey("UpdatedById")
                   .HasConstraintName("FK_CharacterType_UpdateBy")
                    .OnDelete(DeleteBehavior.Restrict);
            });
            modelBuilder.Entity<Character>(entity =>
            {
                entity.HasOne(d => d.UpdatedBy)
                   .WithMany()
                   .HasForeignKey("UpdatedById")
                   .HasConstraintName("FK_Character_UpdateBy")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(d => d.CharacterType)
               .WithMany()
               .HasForeignKey("CharacterTypeId")
               .HasConstraintName("FK_Character_CharacterType")
                .OnDelete(DeleteBehavior.Restrict);
            });


            modelBuilder.Entity<ApplicationUser>(b =>
            {
                // Each User can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.User)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            modelBuilder.Entity<ApplicationRole>(b =>
            {
                // Each Role can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.Role)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();
            });

            new DbInitializer(modelBuilder).Seed();
        } 

    }
}
