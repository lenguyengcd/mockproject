﻿using MediatR;
using MockProject.Domain.Handlers.CharacterTypes;
using System.Net;
using System.Net.Mime;

namespace MockProject.Api.Endpoints
{
    public class CharacterTypeEndpoint : IEndpoint
    {
        private const string BaseEndpoint = "/character-types";

        public IEndpointRouteBuilder MapEndpoints(IEndpointRouteBuilder endpoints)
        {
            endpoints.MapGet(BaseEndpoint, GetCharacterTypesAsync)
                 .WithTags("Character Type")
                 .RequireAuthorization()
                 .Produces((int)HttpStatusCode.OK, typeof(GetCharacterTypesResponse), MediaTypeNames.Application.Json);
            return endpoints;
        }

        private async ValueTask<IResult> GetCharacterTypesAsync(IMediator mediator)
        {
            var response = await mediator.Send(new GetCharacterTypesQuery());
            if (!response.Success)
                return Results.BadRequest(response);
            return Results.Ok(response);
        }
    }
}
