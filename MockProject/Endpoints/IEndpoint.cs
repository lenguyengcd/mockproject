﻿namespace MockProject.Api.Endpoints
{
    public interface IEndpoint
    {
        IEndpointRouteBuilder MapEndpoints(IEndpointRouteBuilder endpoints);
    }
}
