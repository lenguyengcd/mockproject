﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using MockProject.Data.Common;
using MockProject.Domain.Handlers.Users;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MockProject.Api.Endpoints
{
    public class UserEndpoint : IEndpoint
    {
        private const string BaseEndpoint = "/users";

        public IEndpointRouteBuilder MapEndpoints(IEndpointRouteBuilder endpoints)
        {
            endpoints.MapPost($"{BaseEndpoint}/signin", SignInAsync).WithTags("Users");
            endpoints.MapPost($"{BaseEndpoint}/signup", SignUpAsync).WithTags("Users");
            endpoints.MapPut(BaseEndpoint + "/{id:Guid}/ban", BannedUserAsync).WithTags("Users")
            .RequireAuthorization(PolicyConst.AdminPolicy);
            endpoints.MapGet(BaseEndpoint, GetUserAsync).WithTags("Users")
.RequireAuthorization(PolicyConst.AdminPolicy);
            return endpoints;
        }

        private async ValueTask<IResult> SignInAsync(IMediator mediator, UserSignInDto model)
        {
            var res = await mediator.Send(new SignInRequest(model));
            return !res.Success ? Results.BadRequest(res) : Results.Ok(res);
        }

        private async ValueTask<IResult> SignUpAsync(IMediator mediator, UserSignUpDto model)
        {
            var res = await mediator.Send(new SignUpRequest(model));
            return !res.Success ? Results.BadRequest(res) : Results.Ok(res);
        }

        private async ValueTask<IResult> BannedUserAsync(IMediator mediator, Guid id)
        {
            var res = await mediator.Send(new BannedUserRequest(id));
            return !res.Success ? Results.BadRequest(res) : Results.Ok(res);
        }

        private async ValueTask<IResult> GetUserAsync(IMediator mediator)
        {
            var res = await mediator.Send(new GetUsersRequest());
            return !res.Success ? Results.BadRequest(res) : Results.Ok(res);
        }
    }
}
