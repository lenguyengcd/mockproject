﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using MockProject.Api.Extensions;
using MockProject.Data.Common;
using MockProject.Domain.Dtos.Character;
using MockProject.Domain.Handlers.Characters;
using System.Net;
using System.Net.Mime;
using System.Security.Claims;

namespace MockProject.Api.Endpoints
{
    public class CharacterEndpoint : IEndpoint
    {
        private const string BaseEndpoint = "/characters";
        public IEndpointRouteBuilder MapEndpoints(IEndpointRouteBuilder endpoints)
        {
            endpoints.MapGet(BaseEndpoint, GetCharactersAsync)
                .WithTags("Character")
                .RequireAuthorization()
                .Produces((int)HttpStatusCode.OK, typeof(GetCharactersResponse), MediaTypeNames.Application.Json);
            endpoints.MapGet(BaseEndpoint + "/{id:Guid}", GetCharacterDetailAsync)
                  .RequireAuthorization()
                .WithTags("Character")
                .Produces((int)HttpStatusCode.OK, typeof(GetCharactersResponse), MediaTypeNames.Application.Json);
            endpoints.MapPost(BaseEndpoint, CreateCharacterAsync)
                  .RequireAuthorization()
                .WithTags("Character")
                .Produces((int)HttpStatusCode.Created, typeof(CreateCharacterResponse), MediaTypeNames.Application.Json)
                .RequireAuthorization();
            endpoints.MapPut(BaseEndpoint + "/{id:Guid}", UpdateCCharacterAsync)
                  .RequireAuthorization()
                .WithTags("Character")
                .Produces((int)HttpStatusCode.OK, typeof(UpdateCharacterResponse), MediaTypeNames.Application.Json)
                .RequireAuthorization();
            endpoints.MapDelete(BaseEndpoint + "/{id:Guid}", DeleteCharacterAsync)
                  .RequireAuthorization(PolicyConst.AdminPolicy)
                .WithTags("Character")
                .Produces((int)HttpStatusCode.OK, typeof(DeleteCharacterResponse), MediaTypeNames.Application.Json)
                .RequireAuthorization();
            return endpoints;
        }

        private async ValueTask<IResult> GetCharactersAsync(IMediator mediator, ClaimsPrincipal claimsPrincipal,
        IAuthorizationService authorizationService)
        {
            var authResult = await authorizationService.AuthorizeAsync(claimsPrincipal, PolicyConst.AdminPolicy);
            Guid? userId= claimsPrincipal.GetUserClaim()?.Id;
            if (authResult.Succeeded)
            {
                userId = null;
            }
            var response = await mediator.Send(new GetCharactersQuery(userId));
            if (!response.Success)
                return Results.BadRequest(response);
            return Results.Ok(response);
        }
        private async ValueTask<IResult> GetCharacterDetailAsync(IMediator mediator, Guid id)
        {
            var response = await mediator.Send(new GetCharacterDetailQuery(id));
            if (!response.Success)
                return Results.BadRequest(response);
            return Results.Ok(response);
        }
        private async ValueTask<IResult> CreateCharacterAsync(IMediator mediator, CreateCharacterDto model)
        {
            var response = await mediator.Send(new CreateCharacterCommand(model));
            if (!response.Success)
                return Results.BadRequest(response);
            return Results.Created(string.Empty, response);
        }
        private async ValueTask<IResult> UpdateCCharacterAsync(IMediator mediator,Guid id, UpdateCharacterDto model)
        {
            var response = await mediator.Send(new UpdateCharacterCommand(id,model));
            if (!response.Success)
                return Results.BadRequest(response);
            return Results.Ok(response);
        }
        private async ValueTask<IResult> DeleteCharacterAsync(IMediator mediator, Guid id)
        {
            var response = await mediator.Send(new DeleteCharacterCommand(id));
            if (!response.Success)
                return Results.BadRequest(response);
            return Results.Ok(response);
        }

    }
}
