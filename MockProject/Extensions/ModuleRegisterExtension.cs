﻿using MockProject.Api.Endpoints;

namespace MockProject.Api.Extensions
{
    public static class ModuleRegisterExtension
    {
        private static void MapRoutes(IEndpointRouteBuilder endpoint, IEndpoint instance)
        {
            instance?.MapEndpoints(endpoint);
        }
        public static void MapEndpoints(this IEndpointRouteBuilder endpoint, params Type[] assemblies)
        {
            foreach (var assembly in assemblies)
            {
                assembly.Assembly.ExportedTypes
                .Where(f => typeof(IEndpoint).IsAssignableFrom(f) && !f.IsInterface && !f.IsAbstract)
                .Select(Activator.CreateInstance)
                .ToList()
                .ForEach(f =>
                {
                    var instance = f as IEndpoint;
                    if (instance is not null)
                    {
                        MapRoutes(endpoint, instance);
                    }
                });
            }
        }
    }
}
