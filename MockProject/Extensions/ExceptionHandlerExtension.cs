﻿using Microsoft.AspNetCore.Diagnostics;
using MockProject.Data.Common;
using System.Text.Json;

namespace MockProject.Api.Extensions
{
    public static class ExceptionHandlerExtension
    {
        static readonly JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
        public static IApplicationBuilder UseJsonExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(exceptionHandlerApp =>
            {
                exceptionHandlerApp.Run(async (HttpContext context) =>
                {
                    var loggerFactory = context.RequestServices.GetService<ILoggerFactory>();
                    var logger = loggerFactory?.CreateLogger("JsonExceptionHandler");
                    var exception = context.Features.Get<IExceptionHandlerPathFeature>();
                    if (logger != null && exception != null)
                    {
                        logger.LogError(new EventId(9999, "InternalServerError"), exception.Error, $"{exception.Endpoint}");
                    }
                    context.Response.StatusCode = StatusCodes.Status500InternalServerError;

                    context.Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Json;
                    var error = new ErrorResponse
                    {
                        Message = "Internal Server Error.",
                        ErrorCode = "INTERNAL_SERVER_ERROR",
                        Success = false
                    };
                    await context.Response.WriteAsync(JsonSerializer.Serialize(error, jsonSerializerOptions));

                });
            });
            return app;
        }
    }
}
internal record ErrorResponse : Response<string>;