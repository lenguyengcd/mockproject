﻿using MockProject.Data.Common;
using MockProject.Model.Models;
using System.Security.Claims;
using System.Text.Json;

namespace MockProject.Api.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public static UserClaim? GetUserClaim(this ClaimsPrincipal claimsPrincipal)
        {
            var userClaim = claimsPrincipal?.Claims?.FirstOrDefault(f => f.Type == UserClaimsConstant.UserInformation)?.Value;
            if (userClaim == null) return null;
            return JsonSerializer.Deserialize<UserClaim?>(userClaim);
        }
    }
}
