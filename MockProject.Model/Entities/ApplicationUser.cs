﻿using MockProject.Model.SeedWork;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
namespace MockProject.Model.Entities
{
    [Table("AspNetUsers")]
    public class ApplicationUser : IdentityUser<Guid>, IEntityBase
    {
        [MaxLength(100)]
        public string FirstName { get; set; } = string.Empty;
        [MaxLength(100)]
        public string LastName { get; set; } = string.Empty;
        public bool IsDeleted { get; set; }
        public bool IsBanned { get; set; }
        public Guid InsertedById { get; set; }
        public Guid UpdatedById { get; set; }
        public DateTime InsertedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}
