﻿using MockProject.Model.SeedWork;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MockProject.Model.Entities
{
    [Table("CharacterType")]
    public class CharacterType: EntityBase
    {
        [MaxLength(255)]
        public string Name { get; set; }
        public int StrengthDefault { get; set; }
        public int MagicDefault { get; set; }
        public int VitalityDefault { get; set; }
        [ForeignKey("InsertedById")]
        public virtual ApplicationUser InsertedBy { get; set; }
        [ForeignKey("UpdatedById")]
        public virtual ApplicationUser UpdatedBy { get; set; }
    }
}
