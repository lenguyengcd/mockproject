﻿using MockProject.Model.SeedWork;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MockProject.Model.Entities
{
    [Table("Character")]
    public class Character : EntityBase
    {
        [MaxLength(255)]
        public string Name { get; set; }
        public int Strength { get; set; }
        public int Magic { get; set; }
        public int Vitality { get; set; }
        public int Point { get; set; }
        public int Level { get; set; }

        public Guid CharacterTypeId { get; set; }
        [ForeignKey("CharacterTypeId")]
        public virtual CharacterType CharacterType { get; set; }
        [ForeignKey("InsertedById")]
        public virtual ApplicationUser InsertedBy { get; set; }
        [ForeignKey("UpdatedById")]
        public virtual ApplicationUser UpdatedBy { get; set; }
    }
}
