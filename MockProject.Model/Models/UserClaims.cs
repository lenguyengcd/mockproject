﻿namespace MockProject.Model.Models
{
    public class UserClaim
    {
        public Guid Id { get; set; }
        public string Username { get; set; } = string.Empty;
        public IEnumerable<string> Roles { get; set; }
    }
}
