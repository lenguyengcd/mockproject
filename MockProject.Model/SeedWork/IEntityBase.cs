﻿namespace MockProject.Model.SeedWork
{
    public interface IEntityBase
    {
        Guid Id { get; set; }
        bool IsDeleted { get; set; }
        Guid InsertedById { get; set; }
        Guid UpdatedById { get; set; }
        DateTime InsertedAt { get; set; }
        DateTime UpdatedAt { get; set; }
    }
}
