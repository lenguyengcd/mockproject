﻿using MediatR;
using MockProject.Data.Common;
using MockProject.Domain.Dtos.Character;
using MockProject.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Domain.Handlers.Characters
{
    internal class GetCharactersHandler : IRequestHandler<GetCharactersQuery, GetCharactersResponse>
    {
        private ICharacterService _characterService;

        public GetCharactersHandler(ICharacterService characterService)
        {
            _characterService = characterService;
        }

        public async Task<GetCharactersResponse> Handle(GetCharactersQuery request, CancellationToken cancellationToken)
        {
            var response = new GetCharactersResponse();
            var characters = await _characterService.GetCharacters(request.UserId,cancellationToken);
            return response with
            {
                Data = characters,
                Success = true
            };
        }
    }
    public record GetCharactersQuery(Guid? UserId) : IRequest<GetCharactersResponse>;

    public record GetCharactersResponse : Response<IEnumerable<CharacterDto>>;
}
