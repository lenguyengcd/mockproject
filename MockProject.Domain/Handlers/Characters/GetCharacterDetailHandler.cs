﻿using MediatR;
using MockProject.Data.Common;
using MockProject.Domain.Dtos.Character;
using MockProject.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Domain.Handlers.Characters
{
    internal class GetCharacterDetailHandler : IRequestHandler<GetCharacterDetailQuery, GetCharacterDetailResponse>
    {
        private ICharacterService _characterService;

        public GetCharacterDetailHandler(ICharacterService characterService)
        {
            _characterService = characterService;
        }

        public async Task<GetCharacterDetailResponse> Handle(GetCharacterDetailQuery request, CancellationToken cancellationToken)
        {
            var response = new GetCharacterDetailResponse();
            var character = await _characterService.GetCharacter(request.Id, cancellationToken);
            return response with
            {
                Data = character,
                Success = character != null
            };
        }
    }
    public record GetCharacterDetailQuery(Guid Id) : IRequest<GetCharacterDetailResponse>;

    public record GetCharacterDetailResponse : Response<CharacterDto>;
}
