﻿using FluentValidation;
using MediatR;
using MockProject.Data.Common;
using MockProject.Domain.Dtos.Character;
using MockProject.Domain.Services;

namespace MockProject.Domain.Handlers.Characters
{
    internal class CreateCharacterHandler : ValidationBaseHandler<CreateCharacterCommand, CreateCharacterResponse, CreateCharacterDto>
    {
        private ICharacterService _characterService;
        public CreateCharacterHandler(IValidator<CreateCharacterDto> validator, ICharacterService characterService) : base(validator)
        {
            _characterService = characterService;
        }

        protected override async ValueTask<CreateCharacterResponse> AfterValidationSuccess(CreateCharacterCommand request, CreateCharacterResponse response, CancellationToken cancellationToken = default)
        {
            var character = await _characterService.CreateAsync(request.Model, cancellationToken);
            return response with
            {
                Data = character,
                Success = character != null
            };   
        }
    }
    public record CreateCharacterCommand(CreateCharacterDto Model) : IValidationRequest<CreateCharacterResponse, CreateCharacterDto>;

    public record CreateCharacterResponse : Response<CharacterDto>;
}
