﻿using MediatR;
using MockProject.Data.Common;
using MockProject.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Domain.Handlers.Characters
{
    internal class DeleteCharacterHandler : IRequestHandler<DeleteCharacterCommand, DeleteCharacterResponse>
    {
        private ICharacterService _characterService;

        public DeleteCharacterHandler(ICharacterService characterService)
        {
            _characterService = characterService;
        }

        public async Task<DeleteCharacterResponse> Handle(DeleteCharacterCommand request, CancellationToken cancellationToken)
        {
            var response = new DeleteCharacterResponse();
            return response with
            {
                Success = await _characterService.DeleteAsync(request.Id)
            };
        }
    }
}
public record DeleteCharacterCommand(Guid Id) : IRequest<DeleteCharacterResponse>;

public record DeleteCharacterResponse : Response<bool>;

