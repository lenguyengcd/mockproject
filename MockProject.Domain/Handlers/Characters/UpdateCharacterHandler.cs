﻿using FluentValidation;
using MockProject.Data.Common;
using MockProject.Domain.Dtos.Character;
using MockProject.Domain.Services;

namespace MockProject.Domain.Handlers.Characters
{
    internal class UpdateCharacterHandler : ValidationBaseHandler<UpdateCharacterCommand, UpdateCharacterResponse, UpdateCharacterDto>
    {
        private ICharacterService _characterService;
        public UpdateCharacterHandler(IValidator<UpdateCharacterDto> validator, ICharacterService characterService) : base(validator)
        {
            _characterService = characterService;
        }
        protected override async ValueTask<UpdateCharacterResponse> AfterValidationSuccess(UpdateCharacterCommand request, UpdateCharacterResponse response, CancellationToken cancellationToken = default)
        {
            var character = await _characterService.UpdateAsync(request.Id, request.Model, cancellationToken);
            return response with
            {
                Data = character,
                Success = character!=null
            };
        }
    }
    public record UpdateCharacterCommand(Guid Id,UpdateCharacterDto Model) : IValidationRequest<UpdateCharacterResponse, UpdateCharacterDto>;

    public record UpdateCharacterResponse : Response<CharacterDto>;
}
