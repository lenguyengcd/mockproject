﻿using MediatR;
using MockProject.Data.Common;
using MockProject.Domain.Dtos.CharacterType;
using MockProject.Domain.Services;
using MockProject.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Domain.Handlers.CharacterTypes
{
    internal class GetCharacterTypesHandler : IRequestHandler<GetCharacterTypesQuery, GetCharacterTypesResponse>
    {
        private ICharacterTypeService _service;

        public GetCharacterTypesHandler(ICharacterTypeService service)
        {
            _service = service;
        }

        public async Task<GetCharacterTypesResponse> Handle(GetCharacterTypesQuery request, CancellationToken cancellationToken)
        {
            var response = new GetCharacterTypesResponse();
            var characters = await _service.GetCharacters(cancellationToken);
            return response with
            {
                Data = characters,
                Success = true
            };
        }
    }
    public record GetCharacterTypesQuery() : IRequest<GetCharacterTypesResponse>;

    public record GetCharacterTypesResponse : Response<IEnumerable<CharacterTypeDto>>;
}
