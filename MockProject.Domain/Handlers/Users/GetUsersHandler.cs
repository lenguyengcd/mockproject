﻿using MediatR;
using MockProject.Data.Common;
using MockProject.Domain.Dtos.User;
using MockProject.Domain.Services;

namespace MockProject.Domain.Handlers.Users
{
    public class GetUsersHandler : IRequestHandler<GetUsersRequest, GetUsersResponse>
    {
        private readonly IUserService _userService;

        public GetUsersHandler(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<GetUsersResponse> Handle(GetUsersRequest request, CancellationToken cancellationToken)
        {
            var response = new GetUsersResponse();
            var rs = await _userService.GetUsers(cancellationToken);
            return response with
            {
                Data = rs,
                Success = true
            };
        }
    }
    public record GetUsersRequest() : IRequest<GetUsersResponse>;


    public record GetUsersResponse : Response<IEnumerable<UserDto>>;
}