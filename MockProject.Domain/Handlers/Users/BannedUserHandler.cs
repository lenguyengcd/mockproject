﻿using MediatR;
using MockProject.Data.Common;
using MockProject.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Domain.Handlers.Users
{
    internal class BannedUserHandler : IRequestHandler<BannedUserRequest, BannedUserResponse>
    {
        private readonly IUserService _userService;

        public BannedUserHandler(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<BannedUserResponse> Handle(BannedUserRequest request, CancellationToken cancellationToken)
        {
            var response = new BannedUserResponse();
            var rs = await _userService.BannedUser(request.Id, cancellationToken);
            return response with
            {
                Data = rs,
                Success = rs
            };
        }
    }
    public record BannedUserRequest(Guid Id) : IRequest<BannedUserResponse>;

    public record BannedUserResponse : Response<Boolean>;
}