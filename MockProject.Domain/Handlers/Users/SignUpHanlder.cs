﻿using FluentValidation;
using MediatR;
using MockProject.Data.Common;
using MockProject.Domain.Services;

namespace MockProject.Domain.Handlers.Users
{
    internal class SignUpHanlder : ValidationBaseHandler<SignUpRequest, SignUpResponse, UserSignUpDto>
    {
        private readonly IUserService _userService;

        public SignUpHanlder(IValidator<UserSignUpDto> validator, IUserService userService) : base(validator)
        {
            _userService = userService;
        }

        protected override async ValueTask<SignUpResponse> AfterValidationSuccess(SignUpRequest request, SignUpResponse response, CancellationToken cancellationToken = default)
        {
            var result = new SignUpResponse();
            return result with
            {
                Data = await _userService.SignUpAsync(request.Model.Username, request.Model.Password, cancellationToken),
                Success = true
            };
        }
    }
    public record SignUpRequest(UserSignUpDto Model) : IValidationRequest<SignUpResponse, UserSignUpDto>;

    public record SignUpResponse : Response<bool>;


}
