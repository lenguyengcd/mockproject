﻿using MediatR;
using MockProject.Data.Common;
using MockProject.Domain.Dtos.User;
using MockProject.Domain.Services;

namespace MockProject.Domain.Handlers.Users
{
    internal class SignInHanlder : IRequestHandler<SignInRequest, SignInResponse>
    {
        private readonly IUserService _userService;

        public SignInHanlder(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<SignInResponse> Handle(SignInRequest request, CancellationToken cancellationToken)
        {
            var response = new SignInResponse();
            var token = await _userService.SignInAsync(request.Model.Username, request.Model.Password);
            return response with
            {
                Data = token,
                Success = token != null
            };
        }
    }
    public record SignInRequest(UserSignInDto Model) : IRequest<SignInResponse>;

    public record SignInResponse : Response<TokenDto>;

    public class UserSignInDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class UserSignUpDto : UserSignInDto
    {

    }
}
