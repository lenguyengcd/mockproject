﻿using FluentValidation;
using MediatR;
using MockProject.Data.Common;

namespace MockProject.Domain.Handlers
{
    internal abstract class ValidationBaseHandler<TRequest, TResponse, TModel> : IRequestHandler<TRequest, TResponse>
      where TRequest : IRequest<TResponse>, IValidationRequest<TResponse, TModel>
      where TResponse : Response, new()
    {
        private IValidator<TModel> Validator { get; }

        protected ValidationBaseHandler(IValidator<TModel> validator)
        {
            Validator = validator;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken)
        {
            var response = new TResponse();
            var validationResult = await Validator.ValidateAsync(request.Model, cancellationToken);
            if (validationResult.IsValid) return await AfterValidationSuccess(request, response, cancellationToken);
            var validationErrors =
                validationResult.Errors.Select(f => new Error(f.ErrorCode, f.ErrorMessage)).ToArray();
            return response with
            {
                Message = ErrorMessages.ValidationMessage,
                ErrorCode = ErrorCodes.ValidationErrorCode,
                Errors = validationErrors
            };
        }

        protected abstract ValueTask<TResponse> AfterValidationSuccess(TRequest request, TResponse response,
            CancellationToken cancellationToken = default);
    }
}
