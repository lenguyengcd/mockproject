﻿using Mapster;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MockProject.Data.Common;
using MockProject.Data.Infrastructure;
using MockProject.Domain.Dtos.User;
using MockProject.Domain.Services;
using MockProject.Model.Entities;
using MockProject.Model.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MockProject.Domain.ServicesImplement
{
    public class UserService : IUserService
    {
        private readonly IMockProjectUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly UserManager<ApplicationUser> _userManager;
        public UserService(IMockProjectUnitOfWork unitOfWork, IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
            _userManager = userManager;
        }

        public async ValueTask<bool> BannedUser(Guid id, CancellationToken cancellationToken = default)
        {
            var user = await _unitOfWork.UserRepository.Gets(x => x.Id == id).FirstOrDefaultAsync(cancellationToken);
            if (user == null || user.IsBanned) return false;
            user.IsBanned = true;
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async ValueTask<IEnumerable<UserDto>> GetUsers(CancellationToken cancellationToken = default)
        {
            var qry = _unitOfWork.UserRepository.Gets(x=>!x.IsDeleted);
            return await qry.ProjectToType<UserDto>().ToListAsync(cancellationToken);
        }

        public async ValueTask<TokenDto> SignInAsync(string username, string password, CancellationToken cancellationToken = default)
        {
            var user = await _userManager.FindByNameAsync(username);
            if (user == null || user.IsBanned || !await _userManager.CheckPasswordAsync(user, password)) return null;
            return GenerateToken(user, user.UserRoles.Select(x => x.Role));
        }

        public async ValueTask<bool> SignUpAsync(string username, string password, CancellationToken cancellationToken = default)
        {
            var user = new ApplicationUser
            {
                UserName = username,
            };
            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded) return false;
            var addRoleResult = await _userManager.AddToRoleAsync(user, RoleConst.User);
            if (!addRoleResult.Succeeded) return false;
            return true;
        }

        private TokenDto GenerateToken(ApplicationUser user, IEnumerable<ApplicationRole> roles)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("UserInformation", System.Text.Json.JsonSerializer.Serialize(new UserClaim()
                {
                   Id=user.Id,
                   Username=user.UserName,
                   Roles=roles.Select(s=>s.Name)
                })),
            };
            if (roles != null && roles.Count() > 0)
            {
                foreach (var role in roles)
                {
                    permClaims.Add(new Claim(ClaimTypes.Role, role.Name));
                }
            }
            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
               _configuration["Jwt:Audience"],
                permClaims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials);
            return new TokenDto
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
                Username = user.UserName,
                Roles = roles.Select(x => x.Name).ToArray()
            };

        }
    }
}
