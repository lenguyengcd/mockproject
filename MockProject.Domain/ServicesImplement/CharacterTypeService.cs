﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using MockProject.Data.Infrastructure;
using MockProject.Domain.Dtos.CharacterType;
using MockProject.Domain.Services;
using MockProject.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Domain.ServicesImplement
{
    internal class CharacterTypeService : ICharacterTypeService
    {
        private readonly IMockProjectUnitOfWork _unitOfWork;

        public CharacterTypeService(IMockProjectUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async ValueTask<IEnumerable<CharacterTypeDto>> GetCharacters(CancellationToken cancellationToken = default)
        {
            return await _unitOfWork.CharacterTypeRepository.Gets().ProjectToType<CharacterTypeDto>().ToListAsync(cancellationToken);
        }
    }
}
