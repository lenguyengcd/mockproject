﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using MockProject.Data.Infrastructure;
using MockProject.Domain.Dtos.Character;
using MockProject.Domain.Services;
using MockProject.Model.Entities;

namespace MockProject.Domain.ServicesImplement
{
    public class CharacterService : ICharacterService
    {
        private readonly IMockProjectUnitOfWork _unitOfWork;

        public CharacterService(IMockProjectUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async ValueTask<CharacterDto> CreateAsync(CreateCharacterDto model, CancellationToken cancellationToken = default)
        {
            var characterType = await _unitOfWork.CharacterTypeRepository.Gets(x => x.Id == model.CharacterTypeId)
                .FirstOrDefaultAsync(cancellationToken);
            var character = model.Adapt<Character>();
            character.Strength = characterType.StrengthDefault;
            character.Magic = characterType.MagicDefault;
            character.Vitality = characterType.VitalityDefault;
            _unitOfWork.CharacterRepository.Add(character);
            await _unitOfWork.SaveChangesAsync();
            return character.Adapt<CharacterDto>();
        }

        public async ValueTask<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var existingCharacter =
               await _unitOfWork.CharacterRepository.Gets().FirstOrDefaultAsync(f => f.Id == id, cancellationToken);
            if (existingCharacter == null) return false;
            _unitOfWork.CharacterRepository.Delete(existingCharacter);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async ValueTask<CharacterDto> GetCharacter(Guid id, CancellationToken cancellationToken = default)
        {
            return await _unitOfWork.CharacterRepository.Gets(f => f.Id == id && !f.IsDeleted).ProjectToType<CharacterDto>().FirstOrDefaultAsync(cancellationToken);
        }

        public async ValueTask<IEnumerable<CharacterDto>> GetCharacters(Guid? userId, CancellationToken cancellationToken = default)
        {
            var qry = _unitOfWork.CharacterRepository.Gets(x=>!x.IsDeleted);
            if(userId.HasValue)
            {
                qry = qry.Where(x=>x.InsertedById == userId);
            }
            return await qry.ProjectToType<CharacterDto>().ToListAsync(cancellationToken);
        }

        public async ValueTask<CharacterDto> UpdateAsync(Guid id, UpdateCharacterDto model, CancellationToken cancellationToken = default)
        {
            var existingCharacter =
                   await _unitOfWork.CharacterRepository.Gets().Include(x => x.CharacterType).FirstOrDefaultAsync(f => f.Id == id, cancellationToken);
            if (existingCharacter == null) return null;
            _ = model.Adapt(existingCharacter);
            _unitOfWork.CharacterRepository.Update(existingCharacter);
            await _unitOfWork.SaveChangesAsync();
            return existingCharacter.Adapt<CharacterDto>();
        }
    }
}
