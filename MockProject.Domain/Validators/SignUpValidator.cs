﻿using FluentValidation;
using MockProject.Domain.Handlers.Users;

namespace MockProject.Domain.Validators
{
    internal class SignUpValidator : AbstractValidator<UserSignUpDto>
    {
        public SignUpValidator()
        {
            RuleFor(f => f.Username).NotEmpty();
            RuleFor(p => p.Password).NotEmpty().WithMessage("Your password cannot be empty")
                             .MinimumLength(8).WithMessage("Your password length must be at least 8.")
                             .Matches(@"[A-Z]+").WithMessage("Your password must contain at least one uppercase letter.")
                             .Matches(@"[a-z]+").WithMessage("Your password must contain at least one lowercase letter.")
                             .Matches(@"[0-9]+").WithMessage("Your password must contain at least one number.")
                             .Matches(@"[\!\?\*\.\@]+").WithMessage("Your password must contain at least one (!? *.).");
        }
    }
}
