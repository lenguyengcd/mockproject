﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using MockProject.Data.Infrastructure;
using MockProject.Domain.Dtos.Character;

namespace MockProject.Domain.Validators
{
    internal class CreateCharacterValidator :AbstractValidator<CreateCharacterDto>
    {
        private readonly IMockProjectUnitOfWork _unitOfWork;
        public CreateCharacterValidator(IMockProjectUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            RuleFor(f => f.Name).NotEmpty();
            RuleFor(f => f.CharacterTypeId).MustAsync(MustExistsAsync)
                .WithMessage(f => $"Character Type {f.CharacterTypeId} is not existing");
        }

        private async Task<bool> MustExistsAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _unitOfWork.CharacterTypeRepository.Gets(x=>x.Id == id).AnyAsync(cancellationToken);
        }
    }
    internal class UpdateCharacterValidator :  AbstractValidator<UpdateCharacterDto>
    {
        private readonly IMockProjectUnitOfWork _unitOfWork;
        public UpdateCharacterValidator(IMockProjectUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            RuleFor(f => f.Name).NotEmpty();
            RuleFor(f => f.CharacterTypeId).MustAsync(MustExistsAsync)
                .WithMessage(f => $"Character Type {f.CharacterTypeId} is not existing");
        }

        private async Task<bool> MustExistsAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _unitOfWork.CharacterTypeRepository.Gets(x => x.Id == id).AnyAsync(cancellationToken);
        }
    }
}
