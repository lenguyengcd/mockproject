﻿using MockProject.Domain.Dtos.Character;

namespace MockProject.Domain.Services
{
    public interface ICharacterService
    {
        ValueTask<CharacterDto> CreateAsync(CreateCharacterDto character, CancellationToken cancellationToken = default);
        ValueTask<CharacterDto> UpdateAsync(Guid id,UpdateCharacterDto character, CancellationToken cancellationToken = default);
        ValueTask<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
        ValueTask<CharacterDto> GetCharacter(Guid id, CancellationToken cancellationToken = default);
        ValueTask<IEnumerable<CharacterDto>> GetCharacters(Guid? UserId,CancellationToken cancellationToken = default);
    }
}
