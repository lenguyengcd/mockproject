﻿using MockProject.Domain.Dtos.CharacterType;
using MockProject.Model.Entities;

namespace MockProject.Domain.Services
{
    internal interface ICharacterTypeService
    {
        ValueTask<IEnumerable<CharacterTypeDto>> GetCharacters(CancellationToken cancellationToken = default);
    }
}
