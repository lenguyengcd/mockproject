﻿using MockProject.Domain.Dtos.User;
using System.Collections.Generic;

namespace MockProject.Domain.Services
{
    public interface IUserService
    {
        ValueTask<TokenDto> SignInAsync(string username,string password, CancellationToken cancellationToken = default);
        ValueTask<bool> SignUpAsync(string username, string password, CancellationToken cancellationToken = default);
        ValueTask<bool> BannedUser(Guid id, CancellationToken cancellationToken = default);
        ValueTask<IEnumerable<UserDto>> GetUsers(CancellationToken cancellationToken = default);
    }
}
