﻿using Mapster;
using MockProject.Domain.Dtos.Character;
using MockProject.Model.Entities;

namespace MockProject.Domain.MapProfiles
{
    internal class CharacterMapProfile : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            MapCharacter(config);
            MapCreateCharacter(config);
        }

        private static void MapCharacter(TypeAdapterConfig config)
        {
            config.NewConfig<Character, CharacterDto>()
                .Map(f=>f.ChacracterTypeName,f=>f.CharacterType.Name);
        }
        private static void MapCreateCharacter(TypeAdapterConfig config)
        {
            config.NewConfig<CreateCharacterDto, Character>()
                .Map(f=>f.Level,f=>1);
        }
    }
}
