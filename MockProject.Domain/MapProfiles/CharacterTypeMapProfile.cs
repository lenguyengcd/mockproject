﻿using Mapster;
using MockProject.Domain.Dtos.Character;
using MockProject.Domain.Dtos.CharacterType;
using MockProject.Model.Entities;

namespace MockProject.Domain.MapProfiles
{
    internal class CharacterTypeMapProfile : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            MapCharacterType(config);
        }

        private static void MapCharacterType(TypeAdapterConfig config)
        {
            config.NewConfig<CharacterType, CharacterTypeDto>();
        }

    }
}
