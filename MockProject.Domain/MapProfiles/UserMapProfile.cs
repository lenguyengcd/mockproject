﻿using Mapster;
using MockProject.Domain.Dtos.User;
using MockProject.Model.Entities;

namespace MockProject.Domain.MapProfiles
{
    internal class UserMapProfile : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            MapUser(config);
        }

        private static void MapUser(TypeAdapterConfig config)
        {
            config.NewConfig<ApplicationUser, UserDto>()
                .Map(f=>f.Username,f=>f.UserName);
        }
    }
}
