﻿using DateOnlyTimeOnly.AspNet.Converters;
using FluentValidation;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Json;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MockProject.Domain.Dtos.Character;
using MockProject.Domain.Handlers.Users;
using MockProject.Domain.Services;
using MockProject.Domain.ServicesImplement;
using MockProject.Domain.Validators;
using System.Security.Claims;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MockProject.Domain
{
    public static partial class DependencyInjection
    {
        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedHost;
                options.KnownNetworks.Clear();
                options.KnownProxies.Clear();
            });
            services.Configure<JsonOptions>(options =>
            {
                options.SerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                options.SerializerOptions.Converters.Add(new JsonStringEnumConverter());
                options.SerializerOptions.Converters.Add(new TimeOnlyJsonConverter());
            });
            services.AddHttpContextAccessor();
            services.AddMediatR(typeof(DependencyInjection).Assembly);
            services.AddScoped<IValidator<CreateCharacterDto>, CreateCharacterValidator>();
            services.AddScoped<IValidator<UpdateCharacterDto>, UpdateCharacterValidator>();
            services.AddScoped<IValidator<UserSignUpDto>, SignUpValidator>();
            services.AddScoped<ICharacterService, CharacterService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICharacterTypeService, CharacterTypeService>();
            TypeAdapterConfig.GlobalSettings.Scan(typeof(DependencyInjection).Assembly);
        }
    }
}