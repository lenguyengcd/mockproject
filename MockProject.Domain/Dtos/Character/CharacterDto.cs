﻿namespace MockProject.Domain.Dtos.Character
{
    public class CharacterDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Strength { get; set; }
        public int Magic { get; set; }
        public int Vitality { get; set; }
        public int Point { get; set; }
        public int Level { get; set; }

        public string ChacracterTypeName { get; set; }
    }
}
