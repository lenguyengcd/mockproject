﻿namespace MockProject.Domain.Dtos.Character
{
    public class CreateCharacterDto
    {
        public string Name { get; set; }
        public Guid CharacterTypeId { get; set; }
    }
}
