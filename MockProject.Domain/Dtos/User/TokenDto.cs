﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockProject.Domain.Dtos.User
{
    public class TokenDto
    {
        public string AccessToken { get; set; }
        public string Username { get; set; }
        public string[] Roles { get; set; }
    }
}
