﻿namespace MockProject.Domain.Dtos.CharacterType
{
    public class CharacterTypeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int StrengthDefault { get; set; }
        public int MagicDefault { get; set; }
        public int VitalityDefault { get; set; }
    }
}
